# Morse Runner: tired of 599 001
#### Maybe you can try this trick

I was getting tired of stations I “work” in Morse Runner giving me 599 001 and then 599 002 and then maybe 599 005. Before I get to serials like 034 a couple of minutes pass each time. I figured out that maybe I can do something about it?

If you are lazy (and laziness can be good) serial.qcb file is ready for you to be downloaded. Please find in text where you should put it.

## Here comes qrq
qrq was designed in a similar fashion like Rufz, which newer version you may (should?) know as RufzXP. qrq was written by Fabian Kurz DJ5CW (ex DJ1YFK, CW Ops number   1566). You can download qrq from https://fkurz.net/ham/qrq.html . After installation it can train you in receiving callsigns faster and faster, just like RufzXP. But, you can also modify its settings like I did.

Please note that I am running Linux, so if you are a Mac or Windows user some things may be different (especially paths to files).

## Initial setup

After starting qrq press F5 to change some settings:
 - Press up / down / left and right arrows to change initial speed of CW
 - Press “c” to change your callsign
 - If you want you can “Allow unlimited F6” (repetitions) with “f” key
 - It would be a good idea to use “Fixed CW speed” with the “s” key.

After you are done save your settings with “F2”.
![](.README_images/dc62cd8d.png)

Go to the settings menu again and press “d” to check where the callsign database is by pressing “d”. As said, I am on linux so my database files are in /usr/share/qrq/ and ~/.qrq/.

## Here comes fun
qcb files are plain text files, so altering them is easy. Just copy “callbase.qcb” to “serial.qcb”. Now open “serial.qcb” with any kind of spreadsheet software. Here I use LibreOffice Calc, but any excel-like will help.

To avoid weird import behavior make sure that you are importing text in the first column.

![](.README_images/59c2f71c.png)

After importing the file type in the formula in cell B1: “=ROUND(RAND()*1000)” and copy it to other cells in column B. After you are done the effect should look like this:

![](.README_images/a6101fe2.png)

Here I am using polish names of functions, so your may be different.

Save your file as CSV-type file under the same name (“serial.qcb”). This is very important.

## Final touches

This is the tricky part.

Open “serial.qcb” file in any kind of notepad software, it should look like this:

![](.README_images/adf37b30.png)

As you can see there is excessive space between callsign and number. You have to change it to “space”. Here is how:
 - Select tabulation character between callsign and number in the first row
 - Copy it to clipboard (ctrl+c on most systems)
 - Choose “search and replace” function
 - Paste (ctrl+v) that character into “search” box
 - Type single space character in “replace with” box
 - Click “replace in whole file”
 
Remember to save the file.

After you are done, start qrq again and select your “serial.qcb” datafile in the settings menu.

## Final thoughts

qrq and Morse Runner are different kinds of software and my idea was to solve one particular problem I have with Morse Runner: 599 001 problem. It is possible to modify Morse Runner so the stations won’t start with 001, but I found this solution easier to make.

It definitely lacks the “feel” of contest software, but it works. In the past I used a similar trick with most common English words. In the future I hope to train with a similar file dedicated for the contest I am going to participate in (i.e. when stations have static exchange groups like US state abbreviation). 

I have not tried it with CWT exchanges (call, name, number), I am afraid that will be too long and may make qrq to work unstable.

If you have any suggestions for such training files -- please let me know.

---

## About
My name is Mike, my callsign is SQ6JNX. As of December 2021 I am still on my way for my CW Ops number.

This document does contain errors and may not describe the process fully. Moreover, English is not my primary language, so this document also contains  language errors. If you find any -- please be so kind and suggest a correction. 

My (encrypted) email address is moc.liamg@xnj6qs. You have to reverse the letters to decrypt it.
